/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;
import java.util.List; 
/**
 *
 * @author ThaiHoan
 */
public interface ProductDao {
     public List<Product> getProductList(); 
    public void saveProduct(Product prod); 
}
