/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author MYHOME
 */
public class MobileProduct {
     String ProductName;
        int UnitPrice;
        int UnitsInStock;
        String Description;
        String Manufacturer;  
        String Category;
        int Condition1;
        String ProductImageFile;

    public MobileProduct() {
        
    }

    public MobileProduct(String ProductName, int UnitPrice, int UnitsInStock, String Description, String Manufacturer, String Category, int Condition1, String ProductImageFile) {
        this.ProductName = ProductName;
        this.UnitPrice = UnitPrice;
        this.UnitsInStock = UnitsInStock;
        this.Description = Description;
        this.Manufacturer = Manufacturer;
        this.Category = Category;
        this.Condition1 = Condition1;
        this.ProductImageFile = ProductImageFile;
        
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String ProductName) {
        this.ProductName = ProductName;
    }

    public int getUnitPrice() {
        return UnitPrice;
    }

    public void setUnitPrice(int UnitPrice) {
        this.UnitPrice = UnitPrice;
    }

    public int getUnitsInStock() {
        return UnitsInStock;
    }

    public void setUnitsInStock(int UnitsInStock) {
        this.UnitsInStock = UnitsInStock;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public String getManufacturer() {
        return Manufacturer;
    }

    public void setManufacturer(String Manufacturer) {
        this.Manufacturer = Manufacturer;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String Category) {
        this.Category = Category;
    }

    public int getCondition1() {
        return Condition1;
    }

    public void setCondition1(int Condition1) {
        this.Condition1 = Condition1;
    }

    public String getProductImageFile() {
        return ProductImageFile;
    }

    public void setProductImageFile(String ProductImageFile) {
        this.ProductImageFile = ProductImageFile;
    }
    
}
