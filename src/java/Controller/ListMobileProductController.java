/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.MobileProduct;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author ThaiHoan
 */
public class ListMobileProductController extends AbstractController {
    
    public ListMobileProductController() {
    }
    
    protected ModelAndView handleRequestInternal(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
        ModelAndView mv= new ModelAndView();
        List<MobileProduct> mobileProducList = new ArrayList<>();
        PageContent pageContent = new PageContent();
        pageContent.setHeaderName("List Mobile Product");
        mv.addObject(pageContent);
                
          if(request.getParameter("submit")!=null) 
        {
             String ProductName=request.getParameter("ProductName");
                       
                Class.forName("com.mysql.jdbc.Driver");
                Connection cn=DriverManager.getConnection("jdbc:mysql://localhost:3306/hoanmobilestore","root","");
            
                String query="select * from products";
                 
            
            
            try (Statement st=cn.createStatement()) {
                ResultSet rs = st.executeQuery(query);
                 
                while (rs.next()) {
                    
                    String ProductName1 = rs.getString("ProductName");
                    int  UnitPrice1 = rs.getInt("UnitPrice");
                    int UnitsInStock1 = rs.getInt("UnitsInStock");
                    String Description1 = rs.getString("Description");
                    String Manufacturer1 = rs.getString("Manufacturer");
                    String Category1 = rs.getString("Category");
                    int Condition1 = rs.getInt("Condition1");
                    String ProductImageFile1 = rs.getString("ProductImageFile");
                   
                    MobileProduct mp = new MobileProduct();
                    mp.setProductName(ProductName1);
                    mp.setUnitPrice(UnitPrice1);
                    mp.setUnitsInStock(UnitsInStock1);
                    mp.setDescription( Description1);
                    mp.setManufacturer(Manufacturer1);
                    mp.setCategory(Category1);
                    mp.setCondition1(Condition1);
                    mp.setProductImageFile(ProductImageFile1);
                    mobileProducList.add(mp);

                    mv.addObject("ProductName", mp.getProductName());
                    mv.addObject("UnitPrice", mp.getUnitPrice());
                    mv.addObject("UnitsInStock", mp.getCategory());
                    mv.addObject("Description", mp.getDescription());                    
                    mv.addObject("Manufacturer", mp.getManufacturer());
                    mv.addObject("Category", mp.getCategory());                    
                    mv.addObject("Condition1", mp.getCondition1());
                    mv.addObject("ProductImageFile", mp.getProductImageFile());

/*                    
                    mv.addObject("ProductName", ProductName1);
                    mv.addObject("UnitPrice", UnitPrice1);
                    mv.addObject("UnitsInStock", UnitsInStock1);
                    mv.addObject("Description", Description1);                    
                    mv.addObject("Manufacturer", Manufacturer1);
                    mv.addObject("Category", Category1);                    
                    mv.addObject("Condition1", Condition1);
                    mv.addObject("ProductImageFile", ProductImageFile1);
*/

                    
                    mv.addObject("msg", "Nguyen Thai Hoan :"+ProductName1);
                 
                }
                
               
                Map<String,Object> allObjectsMap1 = new HashMap<String,Object>();
                allObjectsMap1.put("allmobileProducListObjects", mobileProducList);
                mv.addAllObjects(allObjectsMap1);
                mv.setViewName("listmobileproducts");
                
               
                
                
                
            } catch (SQLException e) {
      
            }
            
            
        }
        
        return mv;
        
    }
    
    /*-----------------------------------------------------*/
    public class PageContent {

    private String headerName;

    public String getHeaderName() {
        return headerName;
    }

    public void setHeaderName(String headerName) {
        this.headerName = headerName;
    }
}
    
   
}
