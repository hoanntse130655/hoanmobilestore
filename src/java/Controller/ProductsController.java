/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.AbstractController;

/**
 *
 * @author MYHOME
 */
public class ProductsController extends AbstractController {
    
    public ProductsController() {
    }
    
    protected ModelAndView handleRequestInternal(
            HttpServletRequest request,
            HttpServletResponse response) throws Exception {
         ModelAndView mv = new ModelAndView();
        mv.addObject("ProductName","Nguyen Duc Huy");
        mv.addObject("UnitPrice",500);
        mv.addObject("UnitsInStock",200);
        mv.addObject("Description","Nha cua Duc Huy");
        mv.addObject("Manufacturer","Made in Viet Nam");
        mv.addObject("Category","Hang chat luong cao");
        mv.addObject("Condition","Hang chat luong");
        mv.addObject("ProductImageFile","http:NguyenDucHuy.huy.jpeg");
        return mv;
    }
    
}
