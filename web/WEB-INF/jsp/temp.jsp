<%-- 
    Document   : listmobileproducts
    Created on : May 26, 2022, 7:34:53 PM
    Author     : ThaiHoan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <h1>Mobile Products List</h1><br><br>
        <form method="post"> 
            Product Name: <input type="text" name="ProductName"/><br><br>
                               
                        
        <input type="submit" name="submit" value="Save"/>
        </form>
        <br>      
        <br>
        Nguyen Duc HUy ${msg}
        <br>
        <br>
        <h1> Mobile Products List</h1>
        <table>
            <tr><th>Product Name</th><th>Unit Price</th><th>Units In Stock</th></tr>
            <tr><th>${ProductName}</th><th>${UnitPrice}</th><th>${UnitsInStock}</th></tr>
        </table>
    </center>
        
       <h1>${pageContent.headerName}</h1>
        <table>
            
            <tr>
                <th>Product Name;</th>
                <th>Unit Price</th>
                <th>Units In Stock</th>
                <th>Description</th>
                <th>Manufacturer</th>
                <th>Category</th>
                <th>Condition1</th>
                <th>Product Image File</th>
            </tr>
   
            <c:forEach var="mobileProduct" items="${allmobileProducListObjects}">
                <tr>
                    <td><c:out value="${mobileProduct.ProductName}" /></td>

                    <td><c:out value="${mobileProduct.UnitPrice}" /></td>

                    <td><c:out value="${mobileProduct.UnitsInStock}" /></td>
                    <td><c:out value="${mobileProduct.Description}" /></td>
                    <td><c:out value="${mobileProduct.Manufacturer}" /></td>
                    <td><c:out value="${mobileProduct.Category}" /></td>
                    <td><c:out value="${mobileProduct.Condition1}" /></td>
                    <td><c:out value="${mobileProduct.ProductImageFile}" /></td>
                </tr>
            </c:forEach>
        </table>  
        
        <br>
        <br>
        <br>
        
         <h1>${pageContent.headerName}</h1>
        <table>
            <tr>
                <th>Model</th>
                <th>Registration No</th>
                <th>Year of Manufacture</th>
            </tr>

            <c:forEach var="car" items="${allCarObjects}">
                <tr>
                    <td><c:out value="${car.model}" /></td>

                    <td><c:out value="${car.regNo}" /></td>

                    <td><c:out value="${car.year}" /></td>
                </tr>
            </c:forEach>
        </table>
    </body>
    
    <!--    ----------------------------- phan test thu         -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <h1>Mobile Products List</h1><br><br>
        <form method="post"> 
            Product Name: <input type="text" name="ProductName"/><br><br>
                               
                        
        <input type="submit" name="submit" value="Xuat data"/>
        </form>
        <br>      
        <br>
        Nguyen Duc HUy ${msg}
        <br>
        <br>
        <h1> Mobile Products List</h1>
        <table>
            <tr><th>Product Name</th>
                <th>Unit Price</th>
                <th>Units In Stock</th>
                <th>Description</th>
                <th>Manufacturer</th>
                <th>Category</th>
                <th>Condition1</th>
                <th>Product Image File</th></tr>
            <tr><th>${ProductName}</th>
                <th>${UnitPrice}</th>
                <th>${UnitsInStock}</th>
                <th>${Description}</th>
                <th>${Manufacturer}</th>
                <th>${Category}</th>
                <th>${Condition1}</th>
                <th>${ProductImageFile}</th>
                
            </tr>
        </table>
    </center>
    
                
<!---        Phan tham khao ve CSS                        -->                
                
<style>
/* Mã CSS */

.left-column {
    width: 70%;
    height: 150px;
    background: #ddd;
    float: left;
}
.right-column {
    width: 30%;
    height: 150px;
    background: #959595;
    float: right;
}

</style>


    <div class="left-column">
    </div>
    
    <div class="right-column">
    </div>
                    
                
                
                
                
                
                
                
                
    </body>
    
    
    
    
</html>
