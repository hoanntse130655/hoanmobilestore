<%-- 
    Document   : listmobileproducts
    Created on : May 26, 2022, 7:34:53 PM
    Author     : ThaiHoan
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
    <center>
        <h1>Mobile Products List</h1><br><br>
        <form method="post"> 
            Product Name    :     <input type="text" name="ProductName"/><br><br>
                               
                        
        <input style="height:30px;width:200px" type="submit" name="submit" value="Search"/>
        </form>
        <br>      
        <br>
        Nguyen Thai Hoan: ${msg}
        <br>
        <br>
        
    </center>
        
       <h1>${pageContent.headerName}</h1>
        <table>
            
            <tr>
                <th>Picture</th>
                <th>Product Name</th>
                <th>Unit Price</th>
                <th>Units In Stock</th>
                <th>Description</th>
                <th>Manufacturer</th>
                <th>Category</th>
                <th>Condition1</th>
                <th>Product Image File</th>
            </tr>
   
            <c:forEach var="mobileProduct" items="${allmobileProducListObjects}">
                <tr>
                    
                    <td><a href="http://localhost:8084/HoanMobileStore/detailmobileproduct.htm?mobilechon=${mobileProduct.getProductName()}"><img style="height:100px;width:150px"  src="https://image.cellphones.com.vn/358x/media/catalog/product/1/1/11_3_1_1.png" alt="Nguyen Thai Hoan"> </a>
                    
                    </td>
                    <td><c:out value="${mobileProduct.getProductName()}" /></td>
                    <td><c:out value="${mobileProduct.getUnitPrice()}" /></td>
                    <td><c:out value="${mobileProduct.getCategory()}" /></td>

                    <td><c:out value="${mobileProduct.getDescription()}" /></td>
                    <td><c:out value="${mobileProduct.getManufacturer()}" /></td>
                    <td><c:out value="${mobileProduct.getCategory()}" /></td>
                    <td><c:out value="${mobileProduct.getCondition1()}" /></td>
                    <td><c:out value="${mobileProduct.getProductImageFile()}" /></td>
                </tr>
                
        
            </c:forEach>
        </table>  
        
        <br>
        <br>
        <br>
        
        
    </body>
   
</html>
