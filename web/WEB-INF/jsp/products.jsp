<%-- 
    Document   : products
    Created on : 22-May-2022, 09:18:27
    Author     : MYHOME
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Add new product</h1>
        Product Name:<input id="idProductName" autocomplete="${ProductName}"  ></input>
    <br>
    <br>
        Unit Price:<input id="idUnitPrice" autocomplete="${UnitPrice}"  ></input>
        <br>
        <br>
        Units In Stock:<input id="idUnitsInStock" autocomplete="${UnitsInStock}"  ></input>
        <br>
        <br>
        Description:<input id="idDescription" autocomplete="${Description}"  ></input>
        <br>
        <br>
        Manufacturer:<input id="idManufacturer" autocomplete="${Manufacturer}"  ></input>
        <br>
        <br>
        Category:<input id="idCategory" autocomplete="${Category}"  ></input>
        <br>
        <br>
        Condition:<input id="idCondition" autocomplete="${Condition}"  ></input>
        <br>
        <br>
        Product Image File:<input id="idProductImageFile" autocomplete="${ProductImageFile}"  ></input>
        <br>
        <br><br><br>
        <br>
        <br>
        
        <button> Add Product</button> 
    </body>
</html>
